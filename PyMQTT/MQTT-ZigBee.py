#!/usr/bin/python

import paho.mqtt.client as mqtt
import serial
import time

message = 'ON'

ser = serial.Serial('/dev/ttyUSB0', 9600, xonxoff=False)

time.sleep(5)

def on_connect(mosq, obj, rc):
	mqttc.subscribe("LOCK", 0)
	print("rc: " + str(rc))

def on_message(mosq, obj, msg):
	global message
	print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))
	message = msg.payload
	#mqttc.pubblish("f2", msg.payload);
	if msg.payload == "1":
		ser.write("1".encode())
		back = "1"
		print back
		mqttc.publish("di3", back);
	else: 
		ser.write("0".encode())
		fore = "0"
		print fore
		mqttc.publish("di3", fore);


def on_publish(mosq, obj, mid):
	print("mid: " + str(mid))

def on_subscribe(mosq, obj, mid, granted_qos):
	print("subscribed: " + str(mid) + " " + str(granted_qos))

def on_log(mosq, obj, level, string):
	print(string)

mqttc = mqtt.Client()

mqttc.on_message = on_message
mqttc.on_connect = on_connect
mqttc.on_publish = on_publish
mqttc.on_subscribe = on_subscribe

mqttc.connect("localhost", 1883, 60)

mqttc.loop_forever()

