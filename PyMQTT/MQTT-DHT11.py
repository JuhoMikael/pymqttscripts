#!/usr/bin/python

'''
Created on Mar 4, 2016

@author: juhosaarinen
'''


import Adafruit_DHT
import paho.mqtt.client as mqtt
import time

message = 'ON'
topic = 're2'
value = 0
sensor = Adafruit_DHT.DHT11
pin = 17



def on_connect(mosq, obj, rc):
    mqttc.subscribe(topic, 0)
    print("rc: " + str(rc))


def on_publish(mosq, obj, mid):
    print("mid: " + str(mid))

def on_subscribe(mosq, obj, mid, granted_qos):
    print("subscribed: " + str(mid) + " " + str(granted_qos))



mqttc = mqtt.Client()

mqttc.on_connect = on_connect
mqttc.on_publish = on_publish


mqttc.connect("192.168.11.64", 1883, 60)

mqttc.loop_start()

while True:
    humidity, temperature = Adafruit_DHT.read_retry(sensor, pin)

    if humidity is not None and temperature is not None:
            print int(temperature)
            value = int(temperature)
            print value
            mqttc.publish(topic,value)
            time.sleep(15)
    else:
            print 'Failed to get reading. Try again!'


